/**
 * 这个文件是模板以外的积木样式定义
 * @param blockly 
 * @param get_module_name_dropdown 
 */

const bitpin = [
  ['P0', '0'], 
  ['P1', '1'], 
  ['P2', '2'], 
  ['P3', '3'], 
  ['P4', '4'], 
  ['P8', '8'], 
  ['P10', '10'], 
  ['P12', '12'],
  ['P13', '13'],
  ['P14', '14'],
  ['P15', '15']
]

const musicmenu = [
  ['DADADADUM', '0'],
  ['ENTERTAINER', '1'],
  ['PRELUDE', '2'],
  ['ODE', '3'],
  ['NYAN', '4'],
  ['RINGTONE', '5'],
  ['FUNK', '6'],
  ['BLUES', '7'],
  ['BIRTHDAY', '8'],
  ['WEDDING', '9'],
  ['FUNERAL', '10'],
  ['PUNCHLINE', '11'],
  ['BADDY', '12'],
  ['CHASE', '13'],
  ['BA_DING', '14'],
  ['WAWAWAWAA', '15'],
  ['JUMP_UP', '16'],
  ['JUMP_DOWN', '17'],
  ['POWER_UP', '18'],
  ['POWER_DOWN', '19']
]

const gesmenu = [
  ['up', '0'],
  ['down', '1'],
  ['left', '2'],
  ['right', '3'],
  ['face up', '4'],
  ['face down', '5'],
  ['freefall', '6'],
  ['3g', '7'],
  ['6g', '8'],
  ['8g', '9'],
  ['shake', '10']
]

const servomenu = [
  ['S1','1'],
  ['S2','2'],
  ['S3','3'],
  ['S4','4'],
  ['S5','5'],
  ['S6','6'],
  ['S7','7'],
  ['S8','8']
]

export function get_custom_block_config(blockly: any, get_module_name_dropdown: Function) {
  return {
    'kittenbot_microbit_robotbit_scroll': {
      message0: '显示文字 %1',
      args0: [
        {
          type: 'input_value',
          name: 'TEXT',
          check: 'String',
          align: 'CENTRE'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C",
    },
    'kittenbot_microbit_robotbit_matrix': {
      message0: '显示图标 %1',
      args0: [
        {
          type: 'input_value',
          name: 'TEXT',
          check: 'String',
          align: 'CENTRE'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C",
    },
    'kittenbot_microbit_robotbit_temperature': {
      message0: '温度 ℃',
      args0: [],
      colour: "#0FBD8C",
      output: 'Number'
    },
    'kittenbot_microbit_robotbit_digiwrite': {
      message0: '数字写 %1 值 %2',
      args0: [
        {
          type: 'field_dropdown',
          name: 'PIN',
          options: bitpin
        },
        {
          type: 'input_value',
          name: 'LEVEL',
          check: 'Number'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C",
    },
    'kittenbot_microbit_robotbit_digiread': {
      message0: '数字读 %1',
      args0: [
        {
          type: 'field_dropdown',
          name: 'PIN',
          options: bitpin,
        }
      ],
      colour: "#0FBD8C",
      output: 'Boolean'
    },
    'kittenbot_microbit_robotbit_analogwrite': {
      message0: '模拟写 %1 值 %2',
      args0: [
        {
          type: 'field_dropdown',
          name: 'PIN',
          options: bitpin
        },
        {
          type: 'input_value',
          name: 'VALUE',
          check: 'Number'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C",
    },
    'kittenbot_microbit_robotbit_analogread': {
      message0: '模拟读 %1',
      args0: [
        {
          type: 'field_dropdown',
          name: 'PIN',
          options: bitpin,
        }
      ],
      colour: "#0FBD8C",
      output: 'Number'
    },
    'kittenbot_microbit_robotbit_pinpull': {
      message0: '引脚 %1 上下拉 %2',
      args0: [
        {
          type: 'field_dropdown',
          name: 'PIN',
          options: bitpin
        },
        {
          type: 'field_dropdown',
          name: 'PULL',
          options: [
            ['PULL_UP', 'PULL_UP'],
            ['PULL_DOWN', 'PULL_DOWN'],
            ['NO_PULL', 'NO_PULL']
          ]
        },
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C",
    },
    'kittenbot_microbit_robotbit_playmusic': {
      message0: '播放音乐 %1',
      args0: [
        {
          type: 'field_dropdown',
          name: 'MUSIC',
          options: musicmenu
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C",
    },
    'kittenbot_microbit_robotbit_tone': {
      message0: '音调频率 %1 延时 %2 毫秒',
      args0: [
        {
          type: 'input_value',
          name: 'FREQ',
          check: 'Number'
        },
        {
          type: 'input_value',
          name: 'LEN',
          check: 'Number'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C",
    },
    'kittenbot_microbit_robotbit_button': {
      message0: '按键 %1',
      args0: [
        {
          type: 'field_dropdown',
          name: 'BUTTON',
          options: [
            ['A', 'A'],
            ['B', 'B'],
            ['A+B', 'AB']
          ]
        }
      ],
      colour: "#0FBD8C",
      output: 'Boolean'
    },
    'kittenbot_microbit_robotbit_imu': {
      message0: '陀螺仪 %1',
      args0: [
        {
          type: 'field_dropdown',
          name: 'DIRECTION',
          options: [
            ['x', 'x'],
            ['y', 'y'],
            ['z', 'z']
          ]
        }
      ],
      colour: "#0FBD8C",
      output: 'Boolean'
    },
    'kittenbot_microbit_robotbit_gesture': {
      message0: '手势 %1',
      args0: [
        {
          type: 'field_dropdown',
          name: 'GESTURE',
          options: gesmenu
        }
      ],
      colour: "#0FBD8C",
      output: 'Number'
    },
    'kittenbot_microbit_robotbit_compass': {
      message0: '指南针方向',
      args0: [],
      colour: "#0FBD8C",
      output: 'Number'
    },
    'kittenbot_microbit_robotbit_radio': {
      message0: '无线开关 %1',
      args0: [
        {
          type: 'field_dropdown',
          name: 'SWITCH',
          options: [
            ['On', '1'],
            ['Off', '0']
          ]
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_radioch': {
      message0: '无线信道 %1',
      args0: [
        {
          type: 'input_value',
          name: 'CHANNEL',
          check: 'Number'
        },
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_radiosend': {
      message0: '无线发送 %1',
      args0: [
        {
          type: 'input_value',
          name: 'TEXT',
          check: 'String'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_radiogot': {
      message0: '无线接收',
      args0: [],
      colour: "#0FBD8C",
      output: 'String'
    },
    'kittenbot_microbit_robotbit_motorrun': {
      message0: '电机 %1 速度 %2',
      args0: [
        {
          type: 'field_dropdown',
          name: 'MOTOR',
          options: [
            ['M1A', '0'],
            ['M1B', '1'],
            ['M2A', '2'],
            ['M2B', '3']
          ]
        },
        {
          type: 'input_value',
          name: 'SPEED',
          check: 'Number'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_motorrundelay': {
      message0: '电机 %1 速度 %2 延时 %3',
      args0: [
        {
          type: 'field_dropdown',
          name: 'MOTOR',
          options: [
            ['M1A', '0'],
            ['M1B', '1'],
            ['M2A', '2'],
            ['M2B', '3']
          ]
        },
        {
          type: 'input_value',
          name: 'SPEED',
          check: 'Number'
        },
        {
          type: 'input_value',
          name: 'DELAY',
          check: 'Number'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_rosbot_firmware_motor_stop': {
      message0: '电机 %1 停止',
      args0: [
        {
          type: 'field_dropdown',
          name: 'MOTOR',
          options: [
            ['M1A', '0'],
            ['M1B', '1'],
            ['M2A', '2'],
            ['M2B', '3']
          ]
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_stepper': {
      message0: '步进电机 M1 %1 ° M2 %2 °',
      args0: [
        {
          type: 'input_value',
          name: 'DEG1',
          check: 'Number'
        },
        {
          type: 'input_value',
          name: 'DEG2',
          check: 'Number'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_servo': {
      message0: '舵机 %1 角度 %2',
      args0: [
        {
          type: 'field_dropdown',
          name: 'SERVO',
          options: servomenu
        },
        {
          type: 'input_value',
          name: 'DEGREE',
          check: 'Number'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_servogeek': {
      message0: 'Geek舵机 %1 角度 %2',
      args0: [
        {
          type: 'field_dropdown',
          name: 'SERVO',
          options: servomenu
        },
        {
          type: 'input_value',
          name: 'DEGREE',
          check: 'Number'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_rgbcolor': {
      message0: 'RGB %1 颜色 %2',
      args0: [
        {
          type: 'input_value',
          name: 'PIX',
          check: 'Number'
        },
        {
          type: 'input_value',
          name: 'COLOR'
        }
      ],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_rgbshow': {
      message0: 'RGB 显示',
      args0: [],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_rgbclear': {
      message0: 'RGB 关闭',
      args0: [],
      inputsInline: true,
      previousStatement: true,
      nextStatement: true,
      colour: "#0FBD8C"
    },
    'kittenbot_microbit_robotbit_distance': {
      message0: '超声波 %1 cm',
      args0: [
        {
          type: 'field_dropdown',
          name: 'PIN',
          options: bitpin
        }
      ],
      colour: "#0FBD8C",
      output: 'Number'
    }
  }
}