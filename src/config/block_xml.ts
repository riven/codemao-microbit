/**
 * 这个文件是自定义在toolbox的样式
 */
export function get_custom_xml() {
  const buildNumXML = function(argName: any, defval?: any, constrain?: any){ 
    constrain = constrain || '-255, 255,';
    defval = defval || '100';
    return `<value name="${argName}">
      <shadow type="math_number">
        <field name="NUM" constraints="${constrain}">${defval}</field>
      </shadow>
    </value>`
  };
  const buildTextXML = function(argName: any, defval?: any){ 
    defval = defval || '100';
    return `<value name="${argName}">
      <shadow type="text">
        <field name="TEXT">${defval}</field>
      </shadow>
    </value>`
  };
  return {
    kittenbot_microbit_robotbit_scroll: {
      xml: buildTextXML('TEXT', 'Hello')
    },
    kittenbot_microbit_robotbit_matrix: {
      xml: buildTextXML('TEXT', '11100001111')
    },
    kittenbot_microbit_robotbit_digiwrite: {
      xml: buildNumXML('LEVEL', 0)
    },
    kittenbot_microbit_robotbit_analogwrite: {
      xml: buildNumXML('VALUE', 0, '0,255,')
    },
    kittenbot_microbit_robotbit_tone: {
      xml: buildNumXML('FREQ', 500, '0,1024,')+buildNumXML('LEN', 500, '0,10000,')
    },
    kittenbot_microbit_robotbit_radioch: {
      xml: buildNumXML('CHANNEL', 7, '0,100,')
    },
    kittenbot_microbit_robotbit_radiosend: {
      xml: buildTextXML('TEXT', 'Hello')
    },
    kittenbot_microbit_robotbit_motorrun: {
      xml: buildNumXML('SPEED')
    },
    kittenbot_microbit_robotbit_motorrundelay: {
      xml: buildNumXML('SPEED')+buildNumXML('DELAY', 1000, "0, 5000,")
    },
    kittenbot_microbit_robotbit_stepper: {
      xml: buildNumXML('DEG1', 360, '-3600,3600,')+buildNumXML('DEG2', -360, '-3600,3600,')
    },
    kittenbot_microbit_robotbit_servo: {
      xml: buildNumXML('DEGREE', 90)
    },
    kittenbot_microbit_robotbit_servogeek: {
      xml: buildNumXML('DEGREE', 90)
    },
    kittenbot_microbit_robotbit_rgbcolor: {
      xml: buildNumXML('PIX', 1)+`<value name="COLOR">
      <shadow type="colour_picker"></shadow>
    </value>`
    }
  };

}